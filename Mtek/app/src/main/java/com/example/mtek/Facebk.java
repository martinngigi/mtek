package com.example.mtek;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Facebk extends AppCompatActivity {
    private Button Ctnfb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebk);

        Ctnfb = (Button) findViewById(R.id.Ctnfb);
        Ctnfb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLogin2();
            }
        });
    }
    public void openLogin2(){
        Intent intent=new Intent(this, MyFbActivity.class);
        startActivity(intent);
    }
}
