package com.example.mtek;

import android.telecom.Call;

public class ServerStatus {

        private String Group;
        private String Artifact;
        private String Version;
        private String Status;

        public String getArtifact() {
            return Artifact;
        }

        public String getGroup() {
            return Group;
        }

        public String getStatus() {
            return Status;
        }

        public String getVersion() {
            return Version;
        }

    }

