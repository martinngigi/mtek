package com.example.mtek;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MyTwitterActivity extends AppCompatActivity {
    private Button TwitterOkbtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_twitter);

        TwitterOkbtn=(Button) findViewById(R.id.TwitterOkbtn);
        TwitterOkbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTwt();
            }
        });
    }
    public void openTwt(){
        Intent i=new Intent(this, Main13Activity.class);
        startActivity(i);
    }

}
