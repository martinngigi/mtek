package com.example.mtek;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MyEmailActivity extends AppCompatActivity {
    private Button EmlOkbtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_email);

        EmlOkbtn= (Button) findViewById(R.id.EmlOkbtn);
        EmlOkbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                opeMyNav();
            }
        });

    }
    public void opeMyNav(){
        Intent intent = new Intent(this, Main13Activity.class);
        startActivity(intent);
    }


}
