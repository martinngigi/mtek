package com.example.mtek;

import android.util.Log;

import java.util.zip.GZIPInputStream;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PingRepository {
    Retrofit repo = new Retrofit.Builder().baseUrl("").addConverterFactory(GsonConverterFactory.create()).build();
    private iPingWebService pingWebSerice;

    public PingRepository(){
        Retrofit repo = new Retrofit.Builder().baseUrl(":https://hillcroftinsurance.com:8445/").addConverterFactory(GsonConverterFactory.create()).build();
        this.pingWebSerice = repo.create(iPingWebService.class);
    }
    public void getStatus() {
        this.pingWebSerice.getStatus().enqueue(new Callback<ServerStatus>() {
            @Override
            public void onResponse(Call<ServerStatus> call, Response<ServerStatus> response) {
                ServerStatus r = response.body();
                //to string - getGroup
                Log.e("iPING", r.getGroup());
            }

            @Override
            public void onFailure(Call<ServerStatus> call, Throwable t) {

            }
        });

    }
}
