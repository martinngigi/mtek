package com.example.mtek;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MyFbActivity extends AppCompatActivity {
    private Button FbOkbtn;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_fb);

        FbOkbtn=(Button) findViewById(R.id.FbOkbtn);
        FbOkbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFb();
            }
        });
    }
    public void openFb(){
        Intent i=new Intent(this, Main13Activity.class);
        startActivity(i);
    }
   

}
