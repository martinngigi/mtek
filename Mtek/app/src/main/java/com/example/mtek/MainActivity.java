package com.example.mtek;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private Button btnLogin;

    private Button btnTwitter;

    private Button Fb;
    private Button Eml;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Eml = (Button) findViewById(R.id.Eml);
        Eml.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openEml();
            }
        });

        Fb = (Button) findViewById(R.id.Fb);
        Fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFb();
            }
        });




        btnTwitter = (Button) findViewById(R.id.btnTwitter);
        btnTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTwitter();
            }
        });



        btnLogin= (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLogin();
            }
        });



    }


    public void openLogin(){
        Intent log=new Intent(this, Login.class);
        startActivity(log);
    }

    public void openTwitter(){
        Intent twitter= new Intent(this, twitter1.class);
        startActivity(twitter);

    }


    public void openFb(){
        Intent intent = new Intent(this, Facebk.class);
        startActivity(intent);
    }
    public void openEml(){
        Intent intent = new Intent(this, Eml.class);
        startActivity(intent);
    }



}
