package com.example.mtek;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Eml extends AppCompatActivity {
    private Button Snp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eml);

        Snp = (Button) findViewById(R.id.Snp);
        Snp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSingup();
            }
        });
    }
    public void openSingup(){
        Intent intent = new Intent(this, MyEmailActivity.class);
        startActivity(intent);
    }
}
