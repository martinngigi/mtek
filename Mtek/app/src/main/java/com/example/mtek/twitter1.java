package com.example.mtek;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class twitter1 extends AppCompatActivity {
private Button twitterBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_twitter1);

        twitterBtn= (Button) findViewById(R.id.twitterBtn);
        twitterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openlogin2();
            }
        });

    }
    public void openlogin2(){
        Intent intent=new Intent(this, MyTwitterActivity.class);
        startActivity(intent);
    }
}
