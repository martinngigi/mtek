package com.example.mtek;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Login extends AppCompatActivity {
    private Button fb1;
    private Button twita1;
    private Button email1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        fb1= (Button) findViewById(R.id.fb1);
        fb1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFb();
            }
        });

        twita1=(Button) findViewById(R.id.twita1);
        twita1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTwita();
            }
        });

        email1=(Button) findViewById(R.id.email1);
        email1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openEmail();
            }
        });


    }
    public void openFb(){
        Intent lgn=new Intent(this, Facebk.class);
        startActivity(lgn);
    }
    public void openTwita(){
        Intent intent=new Intent(this, twitter1.class);
        startActivity(intent);
    }
    public void openEmail(){
        Intent intent=new Intent(this, Eml.class);
        startActivity(intent);
    }

}
